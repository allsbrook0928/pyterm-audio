# Date: 24/05/2023
# Author: Noah Allsbrook

import sys
import ptaudio # This is the ptaudio.py file in this folder
import pygame

print(" #######################")
print("#                       #")
print("#  PyTerm Audio Player  #")
print("#                       #")
print(" #######################\n")

interrupt_error = "\nKeyboard interrupt! Exiting..."
nofile_error = "\nFile not found."

# Ask what the user wants to do
print("# Options #\n")
print("p - Play a file")
print("f - Play through a folder")
print("s - Shuffle through a folder")
print("e - Exit\n")

try:
    selection = input("What would you like to do: ")
except KeyboardInterrupt:
    print(interrupt_error)
    sys.exit()

# Functions for user input, will add audio player functionality later (maybe in separate python files?)
def choice_play():
    try:
        file_path = input("[PLAY] Enter the path to the file you wish to play: ")
    except KeyboardInterrupt:
        print(interrupt_error)
        sys.exit()

    try:
        ptaudio.play_audio(file_path)
        ptaudio.check_options()
    except pygame.error:
        print(nofile_error)
        sys.exit()

def choice_folder():
    try:
        input("[NO SHUFFLE] Enter the path to the directory you wish to play: ")
    except KeyboardInterrupt:
        print(interrupt_error)
        sys.exit()

    print("This feature is still in development and is not functional at the moment.")

def choice_shuffle():
    try:
        input("[SHUFFLE] Enter the path to the directory you wish to play: ")
    except KeyboardInterrupt:
        print(interrupt_error)
        sys.exit()

    print("This feature is still in development and is not functional at the moment.")

def choice_exit():
    sys.exit()

# Checks input from user to determine function
def check_input(user_in):
    if user_in.lower() == 'p':
        choice_play()
    elif user_in.lower() == 'f':
        choice_folder()
    elif user_in.lower() == 's':
        choice_shuffle()
    elif user_in.lower() == 'e':
        choice_exit()
    else:
        print("Invalid input.")

check_input(selection)
