import pygame
import sys

interrupt_error = "\nKeyboard interrupt! Exiting..."
nofile_error = "\nFile not found.\n"
pygame.mixer.init()

def queue_audio(path):
    pygame.mixer.music.queue(path)

def check_options():
    playing = True

    print("# Options #\n")
    print("p - Pause")
    print("r - Resume")
    print("q - Queue")
    print("e - Exit\n")

    selection = input("What would you like to do: ")

    while playing:
        if selection.lower() == 'p':
            if playing:
                pygame.mixer.music.pause()
                print("Paused.")
               
                playing = False

                try:
                    resume = input()
                except KeyboardInterrupt:
                    print(interrupt_error)
                    sys.exit()
                if resume.lower() == 'r':
                    pygame.mixer.music.unpause()
                    print("Resuming...")
                    
                    playing = True
                    try:
                        selection = input()
                    except KeyboardInterrupt:
                        print(interrupt_error)
                        sys.exit()

        if selection.lower() == 'r':
            selection = input()
        if selection.lower() == 'q':
            file = input("[QUEUE] Enter the path to the file you wish to play: ")
            
            try:
                queue_audio(file)
                print(f"Queued {file}.\n")                
            except pygame.error:
                print(nofile_error + '\n')

            check_options()
        if selection.lower() == 'e':
            pygame.mixer.music.unload()
            sys.exit()

def play_audio(path):
    pygame.mixer.music.load(path)
    pygame.mixer.music.play()

    print(f"Playing file {path}...\n")
    playing = True

