# pyterm-audio
## Name
PyTerm Audio Player

## Description
This is a CLI audio player written in Python.

## Installation
Make sure Python 3 is installed. Download the source code, extract it, and run the Python script as you would any other.

Dependencies:
```
pip3 install pygame
```

## Usage
```
python player.py
```
This runs the main script. Arguments are single characters and directories.

## Support
Bugs can be added to this repo's Issues tab on GitLab. A Discord server is in the works as well.

## Roadmap
- [x] Get basic CLI working.
- [x] Make it play sound from files.
- [x] Add play and pause functionality.
- [x] Add ability to queue files.
- [ ] Add ability to play from folders.
- [ ] Add skip forward and back functions.
- [ ] Add shuffle option when playing from folders.
- [ ] Create GUI (PyQT would be fine I think) - KEEP CLI VERSION ALSO

## Contributing
Contributions are welcome! Just check the description for any unimplemented features.
Ideally you would use Python to contribute but if you want to integrate another language, I would not be opposed to it.

## Authors and acknowledgment
Noah Allsbrook - Author

## License
This project is licensed under the GNU General Public License (v3).

## Project status
Development is active but slow, as I am still learning how to write code.
